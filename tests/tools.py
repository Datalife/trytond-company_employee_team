# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from proteus import Model
from trytond.modules.company.tests.tools import get_company
from datetime import date


def _create_team(name):
    Team = Model.get('company.employee.team')

    team = Team(name=name)
    team.save()

    return team


def add_employees_to_team(team, parties_data, config=None):
    Party = Model.get('party.party')
    Employee = Model.get('company.employee')
    for p in parties_data:
        party = Party(**p)
        party.save()
        employee = Employee(party=party.id, company=get_company())
        employee.save()
        alloc = team.employee_allocations.new()
        alloc.employee = employee
        alloc.date = date(day=1, month=1, year=1900)
    team.save()


def create_team(
        name='A Team',
        parties_data=[{'name': 'Pepito Perez'}],
        config=None):
    team = _create_team(name)
    add_employees_to_team(team, parties_data=parties_data)
    return team


def get_team(config=None):
    """Return the only team"""
    team_pool = Model.get('company.employee.team')
    team = team_pool.find()
    return team[0] if team else None
